/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {StyleSheet, TextInput, TouchableOpacity, Text, View, Image} from 'react-native';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      textValue: '',
      count: 0
    }
  }

  changeTextInput = text => {
    this.setState({value: text});
  };

  onPress = () => {
    this.setState({
      count: this.state.count + 1,
    });
  };

  render(){
    return(
      <View style={styles.container}>
        <View style={styles.text}>
          <Text>
              Hola Amigo
          </Text>
        </View>
        
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <Image source={require('./img/logo.png')}></Image>
        </View>

        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1}}
          onChangeText={text => this.changeTextInput(text)}
          value={this.state.textValue}
        />

        <TouchableOpacity style={styles.button} onPress={this.onPress}>
          <Text>
            Touch Here
          </Text>
        </TouchableOpacity>
        <View style={[styles.countContainer]}>
          <Text style={[styles.countText]}>
            {this.state.count}
          </Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  text: {
    alignItems: 'center',
    padding: 10,
  },

  button: {
    top: 10,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
  }
})

export default App;